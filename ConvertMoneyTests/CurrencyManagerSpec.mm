#import <Cedar/Cedar.h>
#import "CurrencyManager.h"

using namespace Cedar::Matchers;
using namespace Cedar::Doubles;

SPEC_BEGIN(CurrencyManagerSpec)

describe(@"CurrencyManager", ^{
    describe(@"#availableCurrencies", ^{
        __block NSArray *capturedResults;
        
        beforeEach(^{
            capturedResults = [CurrencyManager availableCurrencies];
        });
        
        it(@"should have the correct currencies", ^{
            expect(capturedResults).to(contain(@"GBP"));
            expect(capturedResults).to(contain(@"EUR"));
            expect(capturedResults).to(contain(@"JPY"));
            expect(capturedResults).to(contain(@"BRL"));
        });
    });
    
    describe(@"#currencySymbolFromCurrencyType:", ^{
        __block NSString *capturedSymbol;
        describe(@"When getting the symbol for GBP", ^{
            beforeEach(^{
                capturedSymbol = [CurrencyManager currencySymbolFromCurrencyType:CurrencyTypeGBP];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedSymbol).to(equal(@"£"));
            });
        });
        
        describe(@"When getting the symbol for EUR", ^{
            beforeEach(^{
                capturedSymbol = [CurrencyManager currencySymbolFromCurrencyType:CurrencyTypeEUR];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedSymbol).to(equal(@"€"));
            });
        });
        
        describe(@"When getting the symbol for JPY", ^{
            beforeEach(^{
                capturedSymbol = [CurrencyManager currencySymbolFromCurrencyType:CurrencyTypeJPY];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedSymbol).to(equal(@"¥"));
            });
        });
        
        describe(@"When getting the symbol for BRL", ^{
            beforeEach(^{
                capturedSymbol = [CurrencyManager currencySymbolFromCurrencyType:CurrencyTypeBRL];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedSymbol).to(equal(@"R$"));
            });
        });
        
        describe(@"When getting the symbol for an unknown currency", ^{
            beforeEach(^{
                capturedSymbol = [CurrencyManager currencySymbolFromCurrencyType:CurrencyTypeUnknown];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedSymbol).to(equal(@"$"));
            });
        });
    });
    
    describe(@"#currencyStringFromCurrencyType:", ^{
        __block NSString *capturedString;
        describe(@"When getting the currency string for GBP", ^{
            beforeEach(^{
                capturedString = [CurrencyManager currencyStringFromCurrencyType:CurrencyTypeGBP];
            });
            
            it(@"should return the correct string", ^{
                expect(capturedString).to(equal(@"UK Pounds"));
            });
        });
        
        describe(@"When getting the currency string for EUR", ^{
            beforeEach(^{
                capturedString = [CurrencyManager currencyStringFromCurrencyType:CurrencyTypeEUR];
            });
            
            it(@"should return the correct string", ^{
                expect(capturedString).to(equal(@"EU Euro"));
            });
        });
        
        describe(@"When getting the currency string for JPY", ^{
            beforeEach(^{
                capturedString = [CurrencyManager currencyStringFromCurrencyType:CurrencyTypeJPY];
            });
            
            it(@"should return the correct string", ^{
                expect(capturedString).to(equal(@"Japan Yen"));
            });
        });
        
        describe(@"When getting the currency string for BRL", ^{
            beforeEach(^{
                capturedString = [CurrencyManager currencyStringFromCurrencyType:CurrencyTypeBRL];
            });
            
            it(@"should return the correct string", ^{
                expect(capturedString).to(equal(@"Brazil Reais"));
            });
        });
        
        describe(@"When getting the currency string for an unknown type", ^{
            beforeEach(^{
                capturedString = [CurrencyManager currencyStringFromCurrencyType:CurrencyTypeUnknown];
            });
            
            it(@"should return the correct string", ^{
                expect(capturedString).to(equal(@"US Dollar"));
            });
        });
    });
    
    describe(@"#currencyTypeFromCurrencyString:", ^{
        __block CurrencyType capturedType;
        
        describe(@"When getting the type for GBP", ^{
            beforeEach(^{
                capturedType = [CurrencyManager currencyTypeFromCurrencyString:@"GBP"];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedType).to(equal(CurrencyTypeGBP));
            });
        });
        
        describe(@"When getting the type for EUR", ^{
            beforeEach(^{
                capturedType = [CurrencyManager currencyTypeFromCurrencyString:@"EUR"];
            });
            
            it(@"should return the correct type", ^{
                expect(capturedType).to(equal(CurrencyTypeEUR));
            });
            
            describe(@"When getting the type for JPY", ^{
                beforeEach(^{
                    capturedType = [CurrencyManager currencyTypeFromCurrencyString:@"JPY"];
                });
                
                it(@"should return the correct type", ^{
                    expect(capturedType).to(equal(CurrencyTypeJPY));
                });
            });
            
            describe(@"When getting the type for BRL", ^{
                beforeEach(^{
                    capturedType = [CurrencyManager currencyTypeFromCurrencyString:@"BRL"];
                });
                
                it(@"should return the correct type", ^{
                    expect(capturedType).to(equal(CurrencyTypeBRL));
                });
            });
            
            describe(@"When getting the type for an unknown currency", ^{
                beforeEach(^{
                    capturedType = [CurrencyManager currencyTypeFromCurrencyString:@"???"];
                });
                
                it(@"should return the correct type", ^{
                    expect(capturedType).to(equal(CurrencyTypeUnknown));
                });
            });
        });
    });
});

SPEC_END
