//
//  Currency.m
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import "Currency.h"
#import "CurrencyManager.h"

@interface Currency()
@property (nonatomic, readwrite) CurrencyType currencyType;
@property (nonatomic, readwrite) NSNumber *currencyExchangeRate;
@property (nonatomic, readwrite) NSDate *currencyExchangeDate;
@end

@implementation Currency

- (instancetype)initWithCurrencyType:(CurrencyType)currencyType
                        exchangeRate:(NSNumber *)exchangeRate
                        exchangeDate:(NSDate *)exchangeDate{
    if (self = [super init]){
        self.currencyType = currencyType;
        self.currencyExchangeRate = exchangeRate;
        self.currencyExchangeDate = exchangeDate;
    }
    return self;
}

- (NSString*)currencySymbol{
    return [CurrencyManager currencySymbolFromCurrencyType:self.currencyType];
}

@end
