//
//  CurrencyManager.h
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Currency.h"

@interface CurrencyManager : NSObject

+ (CurrencyManager *)shared;
+ (NSArray <NSString *> *)availableCurrencies;
+ (NSString *)currencySymbolFromCurrencyType:(CurrencyType)currencyType;
+ (NSString *)currencyStringFromCurrencyType:(CurrencyType)currencyType;
+ (CurrencyType)currencyTypeFromCurrencyString:(NSString *)currencyString;

@end
