//
//  Currency.h
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CurrencyType) {
    CurrencyTypeGBP,
    CurrencyTypeEUR,
    CurrencyTypeJPY,
    CurrencyTypeBRL,
    CurrencyTypeUnknown,
    CurrencyTypeTotal
};

@interface Currency : NSObject

@property (nonatomic, readonly) CurrencyType currencyType;
@property (nonatomic, readonly) NSNumber *currencyExchangeRate;
@property (nonatomic, readonly) NSDate *currencyExchangeDate;

- (instancetype)initWithCurrencyType:(CurrencyType)currencyType
                        exchangeRate:(NSNumber *)exchangeRate
                        exchangeDate:(NSDate *)exchangeDate;

- (NSString *)currencySymbol;

@end
