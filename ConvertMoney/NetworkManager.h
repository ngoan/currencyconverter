//
//  NetworkManager.h
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Currency;

@interface NetworkManager : NSObject

+ (NetworkManager*)shared;

- (void)getCurrentExchangeRateWithCompletion:(void (^)(NSArray <Currency *> *currencies, id error))completion;

@end
