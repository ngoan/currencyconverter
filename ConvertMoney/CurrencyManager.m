//
//  CurrencyManager.m
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import "CurrencyManager.h"

@interface CurrencyManager()
@property (nonatomic) NSArray *availableCurrencies;
@end

@implementation CurrencyManager

+ (CurrencyManager *)shared{
    static CurrencyManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

+ (NSArray <NSString *> *)availableCurrencies{
    return @[ @"GBP", @"EUR", @"JPY", @"BRL" ];
}

+ (NSString *)currencySymbolFromCurrencyType:(CurrencyType)currencyType{
    switch (currencyType) {
        case CurrencyTypeGBP:
            return @"£";
        case CurrencyTypeEUR:
            return @"€";
        case CurrencyTypeJPY:
            return @"¥";
        case CurrencyTypeBRL:
            return @"R$";
        default:
            return @"$";
    }
}

+ (NSString *)currencyStringFromCurrencyType:(CurrencyType)currencyType{
    switch (currencyType) {
        case CurrencyTypeGBP:
            return @"UK Pounds";
        case CurrencyTypeEUR:
            return @"EU Euro";
        case CurrencyTypeJPY:
            return @"Japan Yen";
        case CurrencyTypeBRL:
            return @"Brazil Reais";
        default:
            return @"US Dollar";
    }
}

+ (CurrencyType)currencyTypeFromCurrencyString:(NSString *)currencyString{
    
    CurrencyType currencyType = CurrencyTypeUnknown;
    
    NSArray *currencySymbols = [self availableCurrencies];
    for(NSUInteger i = 0; i < currencySymbols.count; i++){
        if([currencyString isEqualToString:currencySymbols[i]]){
            currencyType = (NSUInteger)i;
            break;
        }
    }
    return currencyType;
}

@end
