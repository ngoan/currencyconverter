//
//  NetworkManager.m
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import "NetworkManager.h"
#import "CurrencyManager.h"
#import "Currency.h"

@implementation NetworkManager

+ (NetworkManager*)shared{
    return [NetworkManager new];
}

- (void)getCurrentExchangeRateWithCompletion:(void (^)(NSArray <Currency *> *currencies, id error))completion{
    
    NSURL *conversionURL = [NSURL URLWithString:@"http://api.fixer.io/latest?base=USD"];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:conversionURL
                                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                  {
                                      if(!error){
                                          NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                          NSDate *currencyDate = [self dateFromString:jsonDictionary[@"date"]];
                                          NSDictionary *rates = jsonDictionary[@"rates"];
                                          NSArray *availableCurrencies = [CurrencyManager availableCurrencies];
                                          NSMutableArray *holdingCurrencies = [NSMutableArray new];
                                          
                                          [availableCurrencies enumerateObjectsUsingBlock:^(NSString *availableCurrency, NSUInteger idx, BOOL *stop) {
                                              NSNumber *rate = rates[availableCurrency];
                                              if(rate){
                                                  CurrencyType currencyType = [CurrencyManager currencyTypeFromCurrencyString:availableCurrency];
                                                  Currency *currency = [[Currency alloc] initWithCurrencyType:currencyType exchangeRate:rate exchangeDate:currencyDate];
                                                  [holdingCurrencies addObject:currency];
                                              }
                                          }];
                                          completion(holdingCurrencies, nil);
                                      }else{
                                          completion(nil, error);
                                      }
                                  }];
    [task resume];
    
}

#pragma mark - Private

- (NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"YYYY-MM-DD";
    return [dateFormatter dateFromString:dateString];
}

@end
