//
//  ViewController.m
//  ConvertMoney
//
//  Created by Ngoan Nguyen on 8/10/16.
//  Copyright © 2016 Partswap, Inc. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "CurrencyManager.h"

@interface ViewController ()<UITextFieldDelegate>
@property (nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic) IBOutlet UITextField *originalTextField;
@property (nonatomic) IBOutlet UITextField *convertedTextField;
@property (nonatomic) IBOutlet UILabel *convertedLabel;
@property (nonatomic) NSArray <Currency *> *currencies;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.segmentedControl removeAllSegments];
    
    [[NetworkManager shared] getCurrentExchangeRateWithCompletion:^(NSArray *currencies, id error) {
        self.currencies = currencies;
        [currencies enumerateObjectsUsingBlock:^(Currency *currency, NSUInteger idx, BOOL * _Nonnull stop) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.segmentedControl insertSegmentWithTitle:currency.currencySymbol atIndex:idx animated:NO];
                self.segmentedControl.selectedSegmentIndex = 0;
                [self updateCurrencyLabel];
            });
        }];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self convertFieldsWithOriginalText:newString];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self resetFields];
}

#pragma mark - Private

- (void)updateCurrencyLabel{
    Currency *currency = self.currencies[self.segmentedControl.selectedSegmentIndex];
    self.convertedLabel.text = [CurrencyManager currencyStringFromCurrencyType:currency.currencyType];
}

- (void)convertFieldsWithOriginalText:(NSString*)originalText{
    Currency *currency = self.currencies[self.segmentedControl.selectedSegmentIndex];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    
    if(self.originalTextField.isFirstResponder){
        formatter.currencySymbol = currency.currencySymbol;
        NSNumber *modifiedAmount = @(originalText.integerValue * currency.currencyExchangeRate.floatValue);
        self.convertedTextField.text = [formatter stringFromNumber:modifiedAmount];
    }else{
        formatter.currencySymbol = nil;
        NSNumber *modifiedAmount = @(originalText.integerValue / currency.currencyExchangeRate.floatValue);
        self.originalTextField.text = [formatter stringFromNumber:modifiedAmount];
    }
    
    [self updateCurrencyLabel];
}

- (void)resetFields{
    self.originalTextField.text = nil;
    self.convertedTextField.text = nil;
    [self updateCurrencyLabel];
}

#pragma mark - Actions

- (IBAction)segmentedControlPressed:(id)sender{
    [self resetFields];
}

@end
